package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here

        // Start Dragon Cave
        System.out.println("You are in a land full of dragons. In front of you," +
                            "\nyou see two caves. In one cave, the dragon is friendly" +
                            "\nand will share his treasure with you. The other dragon" +
                            "\nis greedy and hungry and will eat you on sight." +
                            "\nWhich cave will you go into? (1 or 2)");

        //Input value 1 or 2
        Scanner Keyboard = new Scanner(System.in);
        System.out.println(Keyboard.nextInt());

        //Result of input value
        System.out.println("\nYou approach the cave..." +
                            "\nIt is dark and spooky..." +
                            "\nA large dragon jumps out in front of you! He opens his jaws and..." +
                            "\nGobbles you down in one bite!");


    }
}
